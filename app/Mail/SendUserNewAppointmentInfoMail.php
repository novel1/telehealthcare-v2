<?php

namespace App\Mail;

use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserNewAppointmentInfoMail extends Mailable
{
    use Queueable, SerializesModels;

    private $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.appointments.user-appointment-info')
            ->subject(__('Your new appointment'))
            ->with([
                'transaction_id' => $this->appointment->transaction_id,
                'date' => $this->appointment->starts_at->locale('el_GR')->timezone($this->appointment->user->timezone)->format('l d F Y'),
                'time' => $this->appointment->starts_at->timezone($this->appointment->user->timezone)->format('H:i'),
                'timezone' => $this->appointment->user->timezone,
            ]);
    }
}
