<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Inertia\Inertia;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     *
     */
    public function index() {
        $users = User::where('role', '!=', 'doctor')->where('role', '!=', 'admin')->get();

        return Inertia::render('Users/Users', [
            'users' => $users,
            'create_url' => URL::route('app.users.index'),
        ]);
    }

    /**
     *
     */
    public function storeUserLocale(Request $request) {
        $request->validate([
            'locale' => 'required|string'
        ]);

        $user = User::find(auth()->user()->id);

        $user->locale = $request->locale;
        $user->save();

        app()->setLocale($request->locale);

        return response(['success' => true]);
    }

    /**
     * Store new user
     *
     * @todo Maybe use Inertia api and return inertia view, but for now just a json response
     */
    public function store(Request $request) {
        $request->validate([
            'name'            => 'required|string',
            'full_name'       => 'required|string',
            'country'         => 'required|string',
            'timezone'        => 'required|string',
            'phone'           => 'required|string',
            'email'           => 'required|email',
            'locale'          => 'nullable|string',
        ]);

        $pwd = Str::random(12);

        $user = User::create([
            'name'          => trim($request->name),
            'full_name'     => trim($request->full_name),
            'country'       => trim($request->country),
            'timezone'      => trim($request->timezone),
            'phone'         => trim($request->phone),
            'email'         => trim($request->email),
            'role'          => 'user',
            'locale'        => $request->locale ?: 'en',
            'password'      => Hash::make($pwd),
        ]);

        return redirect()
            ->route('app.users.index')
            ->with('success', true)
            ->with('message', ['text' => 'User created!', 'email' => $user->email, 'index' => $pwd]);
    }

    /**
     *
     *
     */
    public function create() {
        return Inertia::render('Users/Create', [
            'create_url' => URL::route('app.users.create'),
        ]);
    }

    /**
     *
     *
     */
    public function show($id) {}

    /**
     * Update
     *
     * @param int $id user id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) {
        $request->validate([
            'id'              => 'required',
            'name'            => 'required|string',
            'full_name'       => 'required|string',
            'country'         => 'required|string',
            'timezone'        => 'required|string',
            'phone'           => 'required|string',
            'email'           => 'required|email',
            'locale'          => 'nullable|string',
        ]);

        $user = User::find($request->id);
        $user->name      = $request->name;
        $user->full_name = $request->full_name;
        $user->country   = $request->country;
        $user->timezone  = $request->timezone;
        $user->phone     = $request->phone;
        $user->email     = $request->email;
        $user->locale    = $request->locale ?: 'en';
        $user->save();

        return response(['user' => $user]);
    }

    public function destroy($id) {}

    public function edit($id) {
        $user = User::findOrFail($id);

        return Inertia::render('Users/Edit', [
            'user_data' => $user->only(['id', 'name', 'full_name', 'email', 'role', 'phone', 'timezone', 'country', 'settings', 'locale']),
            'create_url' => URL::route('app.users.edit', $id)
        ]);
    }
}
