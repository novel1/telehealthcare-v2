@component('mail::message')
# New appointment
<br/><br/>
New appointment scheduled for:
<br>
{{ $date }} {{ __('at') }} {{$time}} ({{ $timezone }})
<br/><br/>
Name: {{$user_name}}
<br/>
Email: {{$user_email}}
<br/>
Phone: {{$user_phone}}
<br/><br/>

@component('mail::button', ['url' => url('/')])
{{ __('go to telehealth.drgeorgallas.com') }}
@endcomponent

<br>
{{ config('app.name') }}
@endcomponent
