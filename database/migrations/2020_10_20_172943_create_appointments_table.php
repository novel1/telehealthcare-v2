<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('transaction_id'); // payment transaction id or internal_book|empty
            $table->string('order_code'); // payment order code or internal_book|empty
            $table->string('status'); // available|scheduled|completed|canceled|pending
            $table->dateTime('starts_at');
            $table->date('start_date');
            $table->time('start_time');
            $table->dateTime('ends_at')->nullable();
            $table->date('end_date')->nullable();
            $table->time('end_time')->nullable();
            $table->dateTime('rescheduled_from')->nullable();
            $table->boolean('reminder_sent')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
