<?php

namespace App\Listeners;

use App\Events\NewAppointmentScheduledFromDoctorEvent;
use App\Mail\SendUserNewAppointmentFromDoctorInfoMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendUserNewAppointmentFromDoctorListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewAppointmentScheduledFromDoctor  $event
     * @return void
     */
    public function handle(NewAppointmentScheduledFromDoctorEvent $event)
    {
        Mail::to($event->appointment->user->email)->send(new SendUserNewAppointmentFromDoctorInfoMail($event->appointment));
    }
}
