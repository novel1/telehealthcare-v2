<?php

namespace App\Listeners;

use App\Events\UserBookedNewAppointmentEvent;
use App\Mail\SendUserNewAppointmentInfoMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendUserNewAppointmentInfoListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserBookedNewAppointment  $event
     * @return void
     */
    public function handle(UserBookedNewAppointmentEvent $event)
    {
        Mail::to($event->appointment->user->email)->send(new SendUserNewAppointmentInfoMail($event->appointment));
    }
}
