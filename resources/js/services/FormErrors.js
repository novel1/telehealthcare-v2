class FormErrors {
    constructor() {
        this.errors = {};
    }

    has() {
        return Object.keys(this.errors).length !== 0;
    }

    get(name) {
        return this.errors[name] || '';
    }

    build(err) {
        if(err.response && err.response.data && err.response.data.errors) {
            let errs = err.response.data.errors;
            Object.keys(errs).forEach(key => {
                this.errors[key] = errs[key][0];
            });
        }
    }

    reset() {
        this.errors = {};
    }

    message(message) {
        return message || 'Something went wrong';
    }
}

export default FormErrors;
