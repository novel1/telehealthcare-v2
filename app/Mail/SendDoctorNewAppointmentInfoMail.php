<?php

namespace App\Mail;

use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDoctorNewAppointmentInfoMail extends Mailable
{
    use Queueable, SerializesModels;

    private $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$timezone = get_setting('admin_timezone');

        return $this->markdown('emails.appointments.doctor-appointment-info')
            ->subject(__('New appointment'))
            ->with([
                'date' => $this->appointment->starts_at->locale('el_GR')->timezone($timezone)->format('l d F Y'),
                'time' => $this->appointment->starts_at->timezone($timezone)->format('H:i'),
                'timezone' => $timezone,
                'user_name' => $this->appointment->user->name,
                'user_email' => $this->appointment->user->email,
                'user_phone' => $this->appointment->user->phone,
            ]);
    }
}
