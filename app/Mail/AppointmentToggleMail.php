<?php

namespace App\Mail;

use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AppointmentToggleMail extends Mailable
{
    use Queueable, SerializesModels;

    private $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.appointments.toggle')
            ->with([
                'date' => $this->displayDate($this->appointment->starts_at),
                'status' => $this->appointment->status,
                'user_name' => $this->appointment->user->name,
                'timezone' => $this->appointment->user->timezone,
            ]);
    }

    /**
     * Build human readable date
     *
     */
    private function displayDate($date) {
        return Carbon::parse($this->appointment->starts_at)
                    ->timezone(trim($this->appointment->user->timezone))
                    ->format('d-m-Y, H:i');
    }
}
