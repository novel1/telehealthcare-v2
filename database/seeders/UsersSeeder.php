<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Kostas';
        $user->full_name = 'Kostas Charal';
        $user->email = 'kostas@noveldigital.pro';
        $user->phone = '99069622';
        $user->country = 'United Kingdom';
        $user->timezone = 'Europe/London';
        $user->password = Hash::make('Kost.1984');
        $user->role = 'doctor';
        $user->settings = json_encode([]);
        $user->remember_token = Str::random(10);
        $user->created_at = now();
        $user->updated_at = now();
        $user->save();

        $user = new User;
        $user->name = 'Constantinos';
        $user->full_name = 'Dr. Constantinos Georgalas';
        $user->email = 'info@drgeorgallas.com';
        $user->phone = '22334455';
        $user->country = 'Cyprus';
        $user->timezone = 'Asia/Nicosia';
        $user->password = Hash::make('ZzerLPN56XfhsxY');
        $user->role = 'doctor';
        $user->settings = json_encode([]);
        $user->remember_token = Str::random(10);
        $user->created_at = now();
        $user->updated_at = now();
        $user->save();

        $user = new User;
        $user->name = 'John';
        $user->full_name = 'John Smith';
        $user->email = 'skapator@gmail.com';
        $user->phone = '22334455';
        $user->country = 'Cyprus';
        $user->timezone = 'Asia/Nicosia';
        $user->password = Hash::make('video2019.test');
        $user->role = 'user';
        $user->settings = json_encode([]);
        $user->remember_token = Str::random(10);
        $user->created_at = now();
        $user->updated_at = now();
        $user->save();
    }
}
