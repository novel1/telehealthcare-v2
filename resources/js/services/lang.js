import { removeAccs } from '@/services/utils';

const langs = {
  day: {
    el: 'ημέρα',
    en: 'day',
  },

  week: {
    el: 'εβδομάδα',
    en: 'week',
  },

  month: {
    el: 'μήνας',
    en: 'month',
  },

  my_account: {
    el: 'Ο Λογαριασμός μου',
    en: 'My Account'
  },

  book_appointment: {
    el: 'Κλείσιμο Ραντεβού',
    en: 'Book Appointment'
  },

  my_appointments: {
    el: 'Τα Ραντεβού μου',
    en: 'My Appointments'
  },

  logout: {
    el: 'Έξοδος',
    en: 'Logout'
  },

  current_password: {
    el: 'Τωρινός κωδικός',
    en: 'Current password'
  },

  new_password: {
    el: 'Νέος κωδικός',
    en: 'New password'
  },

  confirm_new_password: {
    el: 'Επιβεβαίωση νέου κωδικού',
    en: 'Confirm new password'
  },

  UPDATE: {
    el: 'ενημέρωση',
    en: 'update'
  },

  password_updated: {
    el: 'Ο κωδικός ενημερώθηκε',
    en: 'Password updated'
  },

  check_correct_current_password: {
    el: 'Ελέγξτε οτι ο τωρινός κωδικός και η Επιβεβαίωση νέου κωδικού είναι σωστός',
    en: 'Make sure that current password and new password confirmation is correct'
  },

  you_have_no_booked_appointments: {
    el: 'Δεν υπάρχουν προγραμματισμένα ραντεβού.',
    en: 'There are no scheduled appointments.'
  },

  welcome_page_title: {
    el: 'καλωσήρθατε',
    en: 'welcome'
  },

  sign_in: {
    en: 'Sign In'
  },

  sign_up: {
    en: 'Sign Up'
  },

  forgot_password: {
    en: 'Forgot Password'
  },

  reset_password: {
    en: 'Reset Password'
  },

  dashboard: {
    el: 'Πίνακας Ελέγχου',
    en: 'Dashboard'
  },

  profile_info: {
    el: 'ΠΛηροφορίες λογαριασμού',
    en: 'Profile Info'
  },

  profile_security: {
    el: 'Ρυθμίσεις ασφάλειας',
    en: 'Profile security'
  },

  account_files: {
    el: 'Αρχεία',
    en: 'Account files'
  },

  reschedule_appointment: {
    el: 'Αλλαγή Ραντεβού',
    en: 'Reschedule Session'
  },

  booked_appointments: {
    el: 'Προγραμματισμένα ραντεβού',
    en: 'Booked Appointments'
  },

  set_available_timeslots: {
    el: 'Προσθέστε διαθέσιμα ραντεβού',
    en: 'Set availability timeslots'
  },

  users_list: {
    el: 'Λίστα χρηστών',
    en: 'User List'
  },

  create_new_user: {
    el: 'Δημιουργία νέου χρήστη',
    en: 'Create new user'
  },

  edit_user: {
    el: 'Επεξεργασία χρήστη',
    en: 'Edit user'
  },

  book_calendar: {
    el: 'Κλείσιμο ραντεβού',
    en: 'Book Calendar'
  },

  appointment_payment: {
    el: 'Πληρωμή ραντεβού',
    en: 'Appointment payment'
  },

  "Appointment preview": {
    el: 'Προεσκόπηση κλήσης',
    en: 'Appointment preview'
  },

  "Hello": {
    el: "Γειά σου",
    en: "Hello"
  },

  appointment_preview_welcome: {
    el: "Αυτή είναι η προεπισκόπηση της κάμεράς σας. Λάβετε μέρος στην συνεδρία όταν είστε έτοιμοι.",
    en: "This your camera preview. Join session when ready."
  },

  appointment_room: {
    el: 'Ραντεβού',
    en: 'Appointment room'
  },

  start_videocall: {
    el: 'έναρξη βιντοκλήσης',
    en: 'start video call'
  },

  no_ssessions_available_for_booking: {
    el: 'Δεν υπάρχουν διαθέσιμες ώρες για ραντεβού.',
    en: 'No sessions available for booking.'
  },

  select_date: {
    el: 'επιλογή ημέρας',
    en: 'Select Date'
  },

  appointment_info: {
    el: 'πληροφορίες ραντεβού',
    en: 'appointment info'
  },

  close: {
    el: 'κλείσιμο',
    en: 'close',
  },

  cancel: {
    el: 'άκυρο',
    en: 'cancel',
  },

  save: {
    el: 'αποθήκευση',
    en: 'save',
  },

  cancel_appointment: {
    el: 'ακύρωση ραντεβού',
    en: 'cancel appointment'
  },

  back: {
    el: 'επιστροφή',
    en: 'back'
  },

  change: {
    el: 'αλλαγή',
    en: 'change'
  },

  pay: {
    el: 'πληρωμή',
    en: 'pay'
  },

  start_appointment: {
    el: 'έναρξη ραντεβού',
    en: 'start appointment'
  },

  complete_appointment: {
    el: 'ολοκλήρωση ραντεβού',
    en: 'complete appointment'
  },

  manage_account: {
    el: 'Διαχείριση Λογαριασμού',
    en: 'Manage Account'
  },

  "Profile": {
    el: "Προφίλ",
    en: "Profile"
  },

  "User created!": {
    el: 'Ο χρήστης αποθηκεύτηκε!',
    en: 'User created!',
  },

  "Profile Information": {
    el: 'Πληροφορίες Προφίλ',
    en: 'Profile Information'
  },

  "Update your account's profile information and email address.": {
    el: "Ενημερώστε τις πληροφορίες του λογαριασμού σας. Όλα τα πεδία είναι απαραίτητα.",
    en: "Update your account's profile information and email address."
  },

  "Name": {
    el: "Όνομα Χρήστη",
    en: "Username"
  },

  "Full Name": {
    el: "Ονοματεπώνυμο",
    en: "Full Name"
  },

  "Email": {
    el: "Email",
    en: "Email"
  },

  "Phone": {
    el: "Τηλέφωνο",
    en: "Phone"
  },

  "Select country": {
    el: "Επιλογή χώρας",
    en: "Select country"
  },

  "Select your timezone": {
    el: "Επιλογή ζώνης ώρας",
    en: "Select your timezone"
  },

  "Choose your timezone to view available session times": {
    el: "επιλέξτε την ζώνη ώρας σας για να βλεπετε σωστά τις ώρες ραντεβού",
    en: "Choose your timezone to view available session times"
  },

  "Please fill out your profile information before booking an appointment.": {
    el: "Παρακαλώ συμπληρώστε τα στοιχεία προφίλ πριν κλέισετε ραντεβού.",
    en: "Please fill out your profile information before booking an appointment."
  },

  "Go to profile": {
    el: "Στοιχεία Προφίλ",
    en: "Go to profile"
  },

  "Saved.": {
    el: "Αποθηκεύτηκε.",
    en: "Saved."
  },

  "Update Password": {
    el: "Αλλαγή κωδικού",
    en: "Update Password"
  },

  "Ensure your account is using a long, random password to stay secure.": {
    el: "Βεβαιωθήτε οτι ο κωδικός σας περιέχει τουλάχιστον 8 τυχαίους χαρακτήρες ή παραπάνω για ασφάλεια.",
    en: "Ensure your account is using a long, random password to stay secure."
  },

  "New Password": {
    el: "Νέος κωδικός",
    en: "New Password"
  },

  "Confirm Password": {
    el: "Επιβεβαίωση νέου κωδικού",
    en: "Confirm Password"
  },

  "Current Password": {
    el: "Τωρινός κωδικός",
    en: "Current Password"
  },

  "Please fill out this field.": {
    el: "Το πεδίο είναι υποχρωτικό.",
    en: "Please fill out this field."
  },

  "My Appointments": {
    el: "Τα Ραντεβού μου",
    en: "My Appointments"
  },

  "Browser Sessions": {
    el: "Συνεδρίες προγράμματος περιήγησης",
    en: "Browser Sessions"
  },

  "browser_sessions_desc": {
    el: "Αποσυνδέστε τις ενεργές συνεδρίες σας σε άλλα προγράμματα περιήγησης και συσκευές.",
    en: "Manage and logout your active sessions on other browsers and devices."
  },

  "browser_sessions_content": {
    el: "Εάν είναι απαραίτητο, μπορείτε να αποσυνδεθείτε από όλες τις ενεργές συνεδρίες του προγράμματος περιήγησης, σε όλες τις συσκευές σας.  Εάν πιστεύετε ότι ο λογαριασμός σας έχει παραβιαστεί, θα πρέπει επίσης να αλλάξε τον κωδικό πρόσβασής σας.",
    en: "If necessary, you may logout of all of your other browser sessions across all of your devices. If you feel your account has been compromised, you should also update your password."
  },

  "Logout Other Browser Sessions": {
    el: "Αποσύνδεση ενεργών συνεδριών",
    en: "Logout Other Browser Sessions"
  },

  "Select language": {
    el: "Επιλογή γλώσσας",
    en: "Select language"
  },

  "Schedule an appointment for": {
    el: "Προγραμματισμός ραντεβού για",
    en: "Schedule an appointment for"
  },

  "Appointment Calendar": {
    el: "Ημερολόγιο ραντεβού",
    en: "Appointment Calendar"
  },

  "No appointments available": {
    el: "Δεν υπάρχουν ραντεβού",
    en: "No appointments available"
  },

  "price": {
    el: "Τιμή",
    en: "price"
  },

  "Your payment was successful!": {
    el: "Η πληρωμή έγινε με επιτυχία!",
    en: "Your payment was successful!"
  },

  "Your appointment is scheduled for": {
    el: "Το ραντεβού σας προγραμματίστικε για τις",
    en: "Your appointment is scheduled for"
  },

  "Your payment and appointment information will be sent to mail": {
    el: "Οι πληροφορίες πληρωμής και τα στοιχεία του ραντεβού θα σταλούν σύντομα στο email σας.",
    en: "Your payment and appointment information will be sent to your mail."
  },

  "Transaction code": {
    el: "Αριθμός συναλλαγής:",
    en: "Transaction code:"
  },

  "Payment id": {
    el: "Αριθμός παραγγελίας:",
    en: "Payment id:"
  },

  "Payment failed!": {
    el: "Δυστηχώς η πληρωμή δεν ήταν επιτυχής.",
    en: "Payment failed!"
  },

  "Back to appointment calendar" : {
    el: "Eπιστορφή στο ημερολόγιο ραντεβού.",
    en: "Back to the appointment calendar"
  },

  "remindEmailUser": {
    el: 'Send email reminder',
    en: 'Send email reminder'
  }
};

const lang = localStorage.getItem('_thc_lang') || 'en';

export default (key, accents = false) => {
  let str = (langs[key] && langs[key][lang]) ? langs[key][lang].toString() : `${key}`;
  return accents ? removeAccs(str, true) : str;
}
