<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting;
        $setting->key = 'admin_email';
        $setting->value = 'info@drgeorgallas.com';
        $setting->created_at = now();
        $setting->updated_at = now();
        $setting->save();

        $setting = new Setting;
        $setting->key = 'appointment_price';
        $setting->value = '10000';
        $setting->created_at = now();
        $setting->updated_at = now();
        $setting->save();

        $setting = new Setting;
        $setting->key = 'product_id';
        $setting->value = 'price_1J8nwQBHeZ6QnRmLfjUK6pVI';
        $setting->created_at = now();
        $setting->updated_at = now();
        $setting->save();

        $setting = new Setting;
        $setting->key = 'product_id_test';
        $setting->value = 'price_1Iuyr4BHeZ6QnRmLX6s1YpI4';
        $setting->created_at = now();
        $setting->updated_at = now();
        $setting->save();

        $setting = new Setting;
        $setting->key = 'admin_timezone';
        $setting->value = 'Asia/Nicosia';
        $setting->created_at = now();
        $setting->updated_at = now();
        $setting->save();
    }
}
