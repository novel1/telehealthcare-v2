<?php

use App\Models\Setting;

/**
 * errorResponse
 *
 */
function errorResponse($message = null, $errors = null, $code = 422) {
    $data = [];
    $data ['message'] = $message ?: 'Oops, something went wrong!';

    if($errors)
        $data[] = is_array($errors) ? $errors : [$errors];

    return response(['errors' => $data], $code);
}

/**
 * Get site settings, wrapper
 *
 */
function get_setting(string $key, $int = false) {
	$setting = Setting::getSetting($key);

	if($int)
		return $setting->value ? (int) $setting->value : null;

	return $setting->value ?: null;
}