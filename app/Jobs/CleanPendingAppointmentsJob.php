<?php

namespace App\Jobs;

use App\Models\Appointment;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CleanPendingAppointmentsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = now()->timezone('UTC')->subMinutes(18)->format('Y-m-d H:i:s');

        $appointments = Appointment::where('status', 'pending')
            ->where('created_at', '<', $now)
            ->get();

        if($appointments) {
            foreach ($appointments as $appointment) {

                Order::create([
                    'user_id' => $appointment->user_id,
                    'transaction_id' => $appointment->transaction_id,
                    'order_code' => $appointment->order_code,
                    'appointment_date' => $appointment->starts_at,
                    'order_date' => $appointment->created_at,
                    'status' => 'idle_pending',
                    'deleted_by' => 1,
                ]);

                $appointment->update([
                    'transaction_id' => 'na',
                    'order_code' => 'na',
                    'status' => 'available',
                    'user_id' => 1
                ]);

                Log::info('CleanPendingAppointmentsJob: Cleared pending payment. Order code: '.$appointment->order_code);
            }
        }

        Log::info('CleanPendingAppointmentsJob: No pending appointment payments');

        return null;
    }
}
