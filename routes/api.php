<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Order;
use App\Models\Appointment;

Route::prefix('v1')->group(function () {
    Route::get('user/{id}', function ($id) {
        return User::findOrFail($id);
    })->middleware('auth:sanctum');
});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
