<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('deleted_by');
            $table->string('transaction_id'); // payment transaction id or internal_book|empty
            $table->string('order_code'); // payment order code or internal_book|empty
            $table->dateTime('appointment_date');
            $table->dateTime('order_date');
            $table->string('status'); // available|scheduled|completed|canceled|pending
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
