<?php

namespace App\Mail;

use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HourlyAppointmentReminderMail extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.appointments.hourly-notification')
            ->subject('Appointment reminder')
            ->with([
                'date' => $this->data['date'],
                'user_name' => $this->data['user_name'],
                'time' => $this->data['time'],
                'timezone' => $this->data['timezone']
            ]);
    }
}
