<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UserController;
use App\Mail\WelcomeMail;
use App\Mail\EmailAppointmentReminderToUser;
use App\Models\Appointment;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

/*
|------------------------------------------------------------------
| Testing routes
|------------------------------------------------------------------
|
*/
Route::prefix('tests')->group(function () {
    Route::get('layout', function() {
        return Inertia::render('Foo');
    });

    Route::get('stripepay', function() {
        return Inertia::render('StripeFoo');
    });

    Route::get('stripepay/success', function() {
        return Inertia::render('StripeSuccess');
    });

    Route::get('stripepay/cancel', function() {
        return Inertia::render('StripeCancel');
    });

    Route::get('users', function() {
        // return User::all();
        app()->setLocale('el');
        return view('auth.login');
    });

    Route::get('settings', function() {
        return get_setting('admin_email');
    });

    Route::get('appointments', function () {
        return Appointment::all();
    });

    Route::get('uri', function(Request $request) {
        return $request->t;
    });

    Route::get('carbon', function() {
        return Carbon::parse(now())->timezone('UTC');
    });

    Route::get('hash', function() {
        return Crypt::encryptString(202011011000);
    });

    Route::get('carbon-diff', function() {
        $dt_now  = Carbon::parse(now())->timezone('UTC');
        $dt_appt = Carbon::parse('2020-10-29 18:40')->timezone('UTC');

        $diff = $dt_now->diffInMinutes($dt_appt);

        if($diff > 15) {
            // return 'not yet';
        }
        else {
            // return 'ok';
        }

        return [
            $dt_now->greaterThan($dt_appt),
            now(),
            $dt_appt
        ];
    });

    Route::get('email', function() {
        Mail::to('email@email.com')->send(new WelcomeMail());
        return new WelcomeMail();
    });

    Route::get('next-appointments', function() {
        $now_plus_hour = Carbon::parse(now())->timezone('UTC')->addHours(1)->format('Y-m-d H:00:00');

        $appointments = Appointment::where('starts_at', $now_plus_hour)
            ->where('status', 'scheduled')
            ->where('reminder_sent', 0)
            ->get();

        return [$appointments, $now_plus_hour];
    });
});


/*
|------------------------------------------------------------------
| App routes
|------------------------------------------------------------------
|
*/
Route::get('/', function () {
    return redirect('login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

/*
|--------------------------------------------------------------------------
| Users
|--------------------------------------------------------------------------
|
*/
Route::get('/app/users/{id}/edit', [UserController::class, 'edit'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.edit');

Route::get('/app/users/create', [UserController::class, 'create'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.create');

Route::get('/app/users/{id}', [UserController::class, 'show'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.show');

Route::put('/app/users/{id}', [UserController::class, 'update'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.update');

Route::delete('/app/users/{id}', [UserController::class, 'destroy'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.destroy');

Route::get('/app/users', [UserController::class, 'index'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.index');

Route::post('/app/users', [UserController::class, 'store'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.store');

Route::post('/app/users/setlocale', [UserController::class, 'storeUserLocale'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('app.users.setlocale');


/*
|--------------------------------------------------------------------------
| Appointments
|--------------------------------------------------------------------------
|
*/


Route::get('/appointments/{id}/room/preview', [AppointmentController::class, 'showRoomPreview'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.room.preview');

Route::post('/appointments/manual/remind', function (Request $request) {

    $app_id = (int) $request->app_id;

    $appointment = Appointment::find($app_id);
    $appointment->user;

    Mail::to($appointment->user->email)->send(new EmailAppointmentReminderToUser($appointment));

    return ['message' => 'Reminder email sent!'];
})
->middleware(['auth:sanctum', 'verified'])
->name('appointments.manual.remind');

Route::get('/appointments/{id}/room', [AppointmentController::class, 'showRoom'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.room');



Route::get('/appointments/{id}/reschedule', [AppointmentController::class, 'reschedule'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.reschedule');

Route::patch('/appointments/reschedule', [AppointmentController::class, 'storeReschedule'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.reschedule.patch');

Route::post('/appointments/availability/remove', [AppointmentController::class, 'removeAvailability'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.availability.remove');

Route::post('/appointments/complete', [AppointmentController::class, 'complete'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.complete');

Route::get('/appointments/book', [AppointmentController::class, 'available'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.book');

Route::get('/appointments/calendar', [AppointmentController::class, 'booked'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.calendar');

Route::get('/appointments/schedule/user/{id}', [AppointmentController::class, 'schedule'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.availability.schedule');

Route::post('/appointments/schedule', [AppointmentController::class, 'scheduleStore'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.availability.schedule.store');

Route::get('/appointments/availability', [AppointmentController::class, 'availability'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.availability');

Route::post('/appointments/availability', [AppointmentController::class, 'setAvailability'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.availability.set');

Route::patch('/appointments/cancel', [AppointmentController::class, 'cancel'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.cancel');

Route::delete('/appointments', [AppointmentController::class, 'destroy'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.destroy');

/**
 * Get user's apppointments
 *
 * @todo Move to UserController and route
 */
Route::get('/appointments/user', [AppointmentController::class, 'userAppointments'])
    ->middleware(['auth:sanctum', 'verified'])
    ->name('appointments.user');

/*
|------------------------------------------------------------------
|   Create viva order
|------------------------------------------------------------------
|
*/
Route::post('/viva/ordersold', function(Request $request) {

    $request->validate([
        'date' => 'required|date'
    ]);

    $date = Carbon::parse($request->date)->timezone('UTC');
    $appointment = Appointment::where('starts_at', $date)
                        ->where('status', 'available')
                        ->firstOrFail();

    // return Appointment::all();
    // The POST URL and parameters
    $request_url  = env('MIX_VIVAP_API_URL');	// demo environment URL
    $merchant_id  = env('MIX_VIVAP_MERCH_ID');
    $api_key      = env('MIX_VIVAP_API_KEY');
    $amount       = get_setting('appointment_price'); // Amount in cents

    //Set some optional parameters
    // (Full list available here:
    // https://developer.vivawallet.com/api-reference-guide/payment-api/#tag/Payments/paths/~1orders/post)
    $allow_recurring = 'false';

    if($request->user()->locale === 'en') {
        $request_lang = 'en-US';
    }
    elseif($request->user()->locale === 'el') {
        $request_lang = 'el-GR';
    }
    else {
        $request_lang = 'en-US';
    }

    $source = env('MIX_VIVAP_CALLBACK_CODE');

    $postargs = 'Amount='.urlencode($amount).'&AllowRecurring='.$allow_recurring.'&RequestLang='.$request_lang.'&SourceCode='.$source;

    // Get the curl session object
    $ch = curl_init();
    // Set the POST options.
    curl_setopt($ch, CURLOPT_URL, $request_url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postargs);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERPWD, $merchant_id.':'.$api_key);
    curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLSv1.2');

    // Do the POST and then close the session
    $response = curl_exec($ch);

    // Separate Header from Body
    $header_len  = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $res_header  = substr($response, 0, $header_len);
    $res_body    =  substr($response, $header_len);

    curl_close($ch);
    print_r(99);

    // Parse the JSON response
    // return response([0, $request_url], 422);
    try {
        if(is_object(json_decode($res_body))) {
            $res_obj = json_decode($res_body);
            print_r(1);
        } else {
            preg_match('#^HTTP/1.(?:0|1) [\d]{3} (.*)$#m', $res_header, $match);
            print_r(2);
            return response(trim($match[1]), 422);
        }
    } catch( Exception $e ) {
        print_r(3);
        return response($e->getMessage(), 422);
    }

    if ($res_obj->ErrorCode == 0) {	//success when ErrorCode = 0
        print_r(4);
        $appointment->update([
            'user_id' => $request->user_id,
            'order_code' => $res_obj->OrderCode,
            'status' => 'pending',
        ]);

        return response(json_encode($res_obj), 200);
        // To simulate a successful payment, use the 19-digit test credit card 4016000000040000012, with a valid expiration date and 111 as CVV2.
    }
    else {
        print_r(5);
        return response($res_obj->ErrorText, 422);
    }
})->middleware('auth:sanctum', 'verified');


Route::post('/viva/orders', function(Request $request) {

    $request->validate([
        'date' => 'required|date'
    ]);

    $date = Carbon::parse($request->date)->timezone('UTC');
    $appointment = Appointment::where('starts_at', $date)
                        ->where('status', 'available')
                        ->firstOrFail();

    // return Appointment::all();
    // The POST URL and parameters
    $request_url  = env('MIX_VIVAP_API_URL');	// demo environment URL
    $merchant_id  = env('MIX_VIVAP_MERCH_ID');
    $api_key      = env('MIX_VIVAP_API_KEY');
    $amount       = get_setting('appointment_price'); // Amount in cents

    $allow_recurring = 'false';

    if($request->user()->locale === 'en') {
        $request_lang = 'en-US';
    }
    elseif($request->user()->locale === 'el') {
        $request_lang = 'el-GR';
    }
    else {
        $request_lang = 'en-US';
    }

    $source = env('MIX_VIVAP_CALLBACK_CODE');

    $postargs = 'Amount='.urlencode($amount).'&AllowRecurring='.$allow_recurring.'&RequestLang='.$request_lang.'&SourceCode='.$source;

    // Get the curl session object
    $session = curl_init($request_url);

    // Set the POST options.
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $postargs);
    curl_setopt($session, CURLOPT_HEADER, true);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_USERPWD, $merchant_id.':'.$api_key);
    curl_setopt($session, CURLOPT_SSL_CIPHER_LIST, 'TLSv1.2');

    // Do the POST and then close the session
    $response = curl_exec($session);

    // Separate Header from Body
    $header_len = curl_getinfo($session, CURLINFO_HEADER_SIZE);
    $res_header = substr($response, 0, $header_len);
    $res_body   =  substr($response, $header_len);

    curl_close($session);

    // print_r([$res_header, $res_body, $request_url, $merchant_id, $api_key, $amount, $source, $request_lang, $postargs]); return 1;

    try {
        if(is_object(json_decode($res_body))) {
            $res_obj = json_decode($res_body);
        } else {
            preg_match('#^HTTP/1.(?:0|1) [\d]{3} (.*)$#m', $res_header, $match);
            return response(trim($match[1]), 422);
        }
    } catch( Exception $e ) {
        return response($e->getMessage(), 422);
    }

    if ($res_obj->ErrorCode == 0) {	//success when ErrorCode = 0
        $appointment->update([
            'user_id' => $request->user_id,
            'order_code' => $res_obj->OrderCode,
            'status' => 'pending',
        ]);

        return response(json_encode($res_obj), 200);
        // To simulate a successful payment, use the 16-digit test credit card 5511070000000020, with a valid expiration date and 111 as CVV2.        // To simulate a successful payment, use the 19-digit test credit card 4016000000040000012, with a valid expiration date and 111 as CVV2.
    }
    else {

        return response($res_obj->ErrorText, 422);
    }
})->middleware('auth:sanctum', 'verified');


/*
|------------------------------------------------------------------
|   Stripe Payment
|------------------------------------------------------------------
|
*/

Route::post('/payment/stripe', function(Request $request) {
    $request->validate([
        'date' => 'required|date'
    ]);

    $date = Carbon::parse($request->date)->timezone('UTC');
    $appointment = Appointment::where('starts_at', $date)
                        ->where('status', 'available')
                        ->firstOrFail();

    $data = [
        'user' => auth()->user()->id,
        'appointment' => $appointment ? $appointment->id : false,
        'appointment_date' => $date,
    ];

    if($appointment) {
        $session = 'oikdjGmv' . '_' . $request->user_id . '_' . $appointment->id;

        $appointment->update([
            'user_id' => $request->user_id,
            'order_code' => $session,
            'status' => 'pending',
        ]);

        return response($data);
    } else {
        return response($data, 422);
    }

})->middleware('auth:sanctum', 'verified');

/*
|------------------------------------------------------------------
|   Stripe Payment redirects
|------------------------------------------------------------------
|
*/

Route::get('/appointment/payment/stripe/success', function(Request $request) {

    $appointment_user = $request->u ?: 0;
    $appointment_id = $request->a ?: 0;
    $appointment_date = $request->d ?: 0;

    $session = 'oikdjGmv' . '_' . $appointment_user . '_' . $appointment_id;

    $appointment = Appointment::where('order_code', $session)->first();

    if($appointment) {
        $appointment->update([
            'transaction_id' => $session,
            'status' => 'scheduled',
        ]);

        event(new \App\Events\UserBookedNewAppointmentEvent($appointment));
    }

    return Inertia::render('PaymentStripeSuccess', [
        'appointment' => $appointment,
        'session' => $session,
    ]);
});

Route::get('/appointment/payment/stripe/cancel', function(Request $request) {

    $appointment_user = $request->u ?: 0;
    $appointment_id = $request->a ?: 0;
    $appointment_date = $request->d ?: 0;

    $session = 'oikdjGmv' . '_' . $appointment_user . '_' . $appointment_id;

    $appointment = Appointment::where('order_code', $session)
        ->where('user_id', $appointment_user)
        ->where('id', $appointment_id)
        ->first();

    Order::create([
        'user_id' => auth()->user()->id,
        'transaction_id' => 'na',
        'order_code' => $session,
        'appointment_date' => $appointment->starts_at,
        'order_date' => $appointment->created_at,
        'status' => 'payment_fail',
        'deleted_by' => auth()->user()->id,
    ]);

    $appointment->update([
        'transaction_id' => 'na',
        'order_code' => 'na',
        'status' => 'available',
    ]);

    return Inertia::render('PaymentStripeCancel', [
        'appointment' => $appointment,
        'session' => $session,
    ]);
});

/*
|------------------------------------------------------------------
|   Payment route
|------------------------------------------------------------------
|
*/
Route::get('/appointments/payment/status/{status}', function(Request $request) {

    // @todo Validation on viva return params produces redicter for some reason
    // $request->validate([
    //     's' => 'required|alpha_num',
    //     // 't' => 'sometimes|required|alpha_num'
    // ]);

    $order_code = $request->s ?: 0;

    $appointment = Appointment::where('order_code', $order_code)->first();
    // $appointment = Appointment::where('order_code', '8701194206111613')->first();

    if($appointment) {
        if($request->status == 1) {
            $appointment->update([
                'transaction_id' => $request->t,
                'status' => 'scheduled',
            ]);

            event(new \App\Events\UserBookedNewAppointmentEvent($appointment));
        }
        else {
            // If status is 2, create a failed order and release appointment availability
            Order::create([
                'user_id' => auth()->user()->id,
                'transaction_id' => 'na',
                'order_code' => $order_code,
                'appointment_date' => $appointment->starts_at,
                'order_date' => $appointment->created_at,
                'status' => 'payment_fail',
                'deleted_by' => auth()->user()->id,
            ]);

            $appointment->update([
                'transaction_id' => 'na',
                'order_code' => 'na',
                'status' => 'available',
            ]);
        }
    }

    return Inertia::render('Appointments/Payment', [
        'status' => (int) $request->status,
        'payment' => $request->all(),
        'appointment' => $appointment,
    ]);
})->middleware('auth:sanctum', 'verified')->where('status',  '[1-2]+');

/*
|------------------------------------------------------------------
|   Get twilio room token
|   @todo move to controller
|------------------------------------------------------------------
|
*/
Route::post('twilio/token', function(Request $request) {
    // Substitute your Twilio AccountSid and ApiKey details
    $accountSid = env('MIX_TWILIO_ACCOUNT_SID');
    //tch-sessions2
    $apiKeySid = env('MIX_TWILIO_API_KEY_SID');
    $apiKeySecret = env('MIX_TWILIO_API_KEY_SECRET');

    $identity = $request->identity;
    $room_name = $request->room_name;

    // Create an Access Token
    $token = new AccessToken(
        $accountSid,
        $apiKeySid,
        $apiKeySecret,
        3600,
        $identity
    );

    // Grant access to Video
    $grant = new VideoGrant();
    $grant->setRoom($room_name);
    $token->addGrant($grant);

    // Serialize the token as a JWT
    return response(['token' => $token->toJWT()]);
    // dd($grant->getPayload());
});

Route::get('/settings/edit', [SettingController::class, 'edit'])->middleware(['auth:sanctum', 'verified'])->name('settings.edit');

Route::patch('/settings', [SettingController::class, 'update'])->middleware(['auth:sanctum', 'verified'])->name('settings.update');

/*
|------------------------------------------------------------------
|   Create order viva sample code
|   https://github.com/VivaPayments/API/tree/master/CreateOrder/PHP
|------------------------------------------------------------------
|
*/
Route::post('/viva/orders/sample', function(Request $request) {

    $request->validate([
        'date' => 'required|date'
    ]);

    //Set some optional parameters (Full list available here: https://developer.vivawallet.com/api-reference-guide/payment-api/#tag/Payments/paths/~1orders/post)
    $AllowRecurring = 'false'; // If set to true, this flag will prompt the customer to accept recurring payments in tbe future.
    $RequestLang = 'en-US'; //This will display the payment page in English (default language is Greek)
    $Source = 'Default'; // This will assign the transaction to the Source with Code = "Default". If left empty, the default source will be used.

    $request_url  = env('MIX_VIVAP_API_URL');   // demo environment URL
    $merchant_id  = env('MIX_VIVAP_MERCH_ID');
    $api_key      = env('MIX_VIVAP_API_KEY');
    $amount       = get_setting('appointment_price'); // Amount in cents
    $allow_recurring = 'false'; // If set to true, this flag will prompt the customer to accept recurring payments in tbe future.
    // @todo Move to user lang
    $request_lang    = 'el-GR'; //This will display the payment page in English (default language is Greek)
    $source          = env('MIX_VIVAP_CALLBACK_CODE');

    $postargs = 'Amount='.urlencode($amount).'&AllowRecurring='.$allow_recurring.'&RequestLang='.$request_lang.'&SourceCode='.$source;

    // Get the curl session object
    $session = curl_init($request_url);


    // Set the POST options.
    curl_setopt($session, CURLOPT_POST, true);
    curl_setopt($session, CURLOPT_POSTFIELDS, $postargs);
    curl_setopt($session, CURLOPT_HEADER, true);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_USERPWD, $merchant_id.':'.$api_key);
    curl_setopt($session, CURLOPT_SSL_CIPHER_LIST, 'TLSv1.2');

    // Do the POST and then close the session
    $response = curl_exec($session);

    // Separate Header from Body
    $header_len = curl_getinfo($session, CURLINFO_HEADER_SIZE);
    $resHeader = substr($response, 0, $header_len);
    $resBody =  substr($response, $header_len);

    curl_close($session);

    // Parse the JSON response
    try {
        if(is_object(json_decode($resBody))){
            $resultObj=json_decode($resBody);
        }else{
            preg_match('#^HTTP/1.(?:0|1) [\d]{3} (.*)$#m', $resHeader, $match);
                    throw new Exception("API Call failed! The error was: ".trim($match[1]));
        }
    } catch( Exception $e ) {
        echo $e->getMessage();
    }

    if ($resultObj->ErrorCode==0){  //success when ErrorCode = 0
        return response(json_encode($resultObj), 200);
    }

    else{
        echo 'The following error occured: ' . $resultObj->ErrorText;
    }
});
