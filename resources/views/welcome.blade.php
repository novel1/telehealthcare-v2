<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TeleHealthCare</title>
        <style>
            body {
                margin: 0;
                padding: 0;
                width: 100vw;
                height: 100vh;
                background: url('/img/bg1.jpg') center center no-repeat;
                background-size: cover;
                display: flex;
                align-items: center;
                justify-content: center;
            }
        </style>
    </head>
    <body class="antialiased">
        <x-jet-authentication-card-logo />
    </body>
</html>
