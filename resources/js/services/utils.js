import { getCanVGrowWithinCell } from "@fullcalendar/vue";

/**
 * Smooth scroll to the top of the page
 *
 */
export const scrollToTop = () => {
  const c = document.documentElement.scrollTop || document.body.scrollTop;
  if (c > 0) {
    window.requestAnimationFrame(scrollToTop);
    window.scrollTo(0, c - c / 8);
  }
};

/**
 * The browser tab of the page is focused
 *
 * @return {Boolean}
 */
const isBrowserTabFocused = () => !document.hidden

/**
 * Get the scroll position of the current page
 *
 * @return { Object }
 */
export const getScrollPosition = (el = window) => ({
  x: el.pageXOffset !== undefined ? el.pageXOffset : el.scrollLeft,
  y: el.pageYOffset !== undefined ? el.pageYOffset : el.scrollTop
});

export const hasClass = (el, className) => el.classList.contains(className);

/**
 * Fetch all images within an elemen
 *
 * @return { Set }
 */
export const getImages = (el, duplicates = false) => {
  const images = [...el.getElementsByTagName('img')].map(img => img.getAttribute('src'));
  return includeDuplicates ? images : [...new Set(images)];
};

/**
 * Is device mobile
 *
 * @return { Boolean }
 */
export const isMobile = () => /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? true : false;

/**
 * Get the current url
 *
 * @return { String }
 */
export const currentUrl = () => window.location.href;

/**
 * Toggle a class for an elementge
 *
 */
export const toggleClass = (el, className) => el.classList.toggle(className);

/**
 * Slugify a string
 *
 * @param  {string} text
 * @return {string} text
 */
export const slugify = (text) => {
  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '_')
    .replace(/[^\w-]+/g, '')
    .replace(/--+/g, '_')
    .replace(/^-+/, '')
    .replace(/-+$/, '');
};

/**
 * Months
 *
 * @return {Array} text
 */
export const months = () => {
  return [
    {name: 'January', value: 0},
    {name: 'February', value: 1},
    {name: 'March', value: 2},
    {name: 'April', value: 3},
    {name: 'May', value: 4},
    {name: 'June', value: 5},
    {name: 'July', value: 6},
    {name: 'August', value: 7},
    {name: 'September', value: 8},
    {name: 'October', value: 9},
    {name: 'November', value: 10},
    {name: 'December', value: 11},
  ];
}

/**
 * Hex color to rgb
 *
 * @param {String} hex Hex color
 * @return {String} text
 */
export const hexToRgb = (hex) => {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ?
    `${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)}`
    : null;
}

/**
 * Does an array have some values of another array
 *
 * @param {Array} arr1
 * @param {Array} arr2
 * @return {Boolean}
 */
export const containSome = (arr1, arr2) => {
  return arr1.some(r => arr2.indexOf(r) >= 0);
}

/**
 * Eencode a set of form elements as an object
 *
 * @param  {HTMLFormElement} form The form element
 * @return {Object]}
 */
export const formToObject = form => Array.from(new FormData(form)).reduce(
  (acc, [key, value]) => ({
    ...acc,
    [key]: value
  }),{}
);

/**
 * [description]
 * @param  {[type]} formElements [description]
 * @return {[type]}              [description]
 */
export const buildFormData = (formElements) => {
  const formData = new FormData();
  for (let i = 0; i < formElements.length; i++) {
    let input = formElements[i];
    // @todo Remove submit button from form
    if(input.id !== 'submit') {
      formData.append(input.id, input.value);
    }
  }

  return formData;
}

/**
 * Copy a string to the clipboard
 *
 * @param  {String} str
 */
export const copyToClipboard = str => {
  const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  const selected =
    document.getSelection().rangeCount > 0 ? document.getSelection().getRangeAt(0) : false;
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  if (selected) {
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(selected);
  }
};

/**
 * Array Chunk
 *
 */
export const arrayChunk = (array, chunks, addOrphansToLastGroup) => {
    addOrphansToLastGroup = addOrphansToLastGroup || true;
    chunks = chunks || 5;

    var groups  = [], i;
    var orphans = array.length % chunks;
    var popped   = null;

    if(addOrphansToLastGroup && orphans < Math.ceil(chunks/2)) {
        popped = array.splice(-orphans, orphans);
    }

    for(i = 0; i < array.length; i += chunks) {
        groups.push(array.slice(i, i + chunks));
    }

    if(popped && popped.length) {
        popped.forEach((p, i) => {
            groups[groups.length-1].push(p);
        });
    }

    return groups;
}

/**
 * If the given value is not an array, wrap it in one.
 *
 * @param  {Any} value
 * @return {Array}
 */
export const arrayWrap = (value) => {
  return Array.isArray(value) ? value : [value]
}

// simpleHash
export const simpleHash = (str) => {
  str = str || randomStr();
  return Array.from(str).reduce((hash, char) => 0 | (31 * hash + char.charCodeAt(0)));
}

export const  randomStr = (length = 16) => {
   var result      = '';
   var chars       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charsLength = chars.length;
   for ( let i = 0; i < length; i++ ) {
      result += chars.charAt(Math.floor(Math.random() * charsLength));
   }
   return result + '_' + Date.now();
}

// Parse a Link header
// Link:<https://example.org/.meta>; rel=meta
// TODO: simplify!
export const parseLinkHeader = (header) => {
  const linkExp = /<[^>]*>\s*(\s*;\s*[^()<>@,;:"/[\]?={} \t]+=(([^()<>@,;:"/[\]?={} \t]+)|("[^"]*")))*(,|$)/g;
  const paramExp = /[^()<>@,;:"/[\]?={} \t]+=(([^()<>@,;:"/[\]?={} \t]+)|("[^"]*"))/g;

  const linkMatch = header.match(linkExp);
  const rels = {};
  for (let i = 0; i < linkMatch.length; i++) {
    const linkSplit = linkMatch[i].split('>');
    const href = linkSplit[0].substring(1);
    const rel = linkSplit[1];
    const relMatch = rel.match(paramExp);
      for (let j = 0; j < relMatch.length; j++) {
        const relSplit = relMatch[j].split('=');
        const relName = relSplit[1].replace(/["']/g, '');
        rels[relName] = href;
      }
  }
  return rels.next || rels.prev ? rels : null;
}

// getQueryString
export const getQueryString = ( name ) => {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  let regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  let results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

// Has own property wrapper
export const objHas = (obj, path) => {
  return obj != null && hasOwnProperty.call(obj, path);
}

// Is a given value an array?
// Delegates to ECMA5's native Array.isArray
export const isArray = Array.isArray || function(obj) {
  return toString.call(obj) === '[object Array]';
};

/**
 * Remove accents
 *
 * @param {String} str
 * @param {Boolean} lowerCase remove from lowercase
 */
export const removeAccs = (str, lowerCase = false) => {

  str = str.replace( /Ά/g, "Α" );
  str = str.replace( /Έ/g, "Ε" );
  str = str.replace( /Ή/g, "Η" );
  str = str.replace( /Ί/g, "Ι" );
  str = str.replace( /Ϊ/g, "Ι" );
  str = str.replace( /Ό/g, "Ο" );
  str = str.replace( /Ύ/g, "Υ" );
  str = str.replace( /Ϋ/g, "Υ" );
  str = str.replace( /Ώ/g, "Ω" );

  if(lowerCase) {
    str = str.replace( /ά/g, "α" );
    str = str.replace( /έ/g, "ε" );
    str = str.replace( /ή/g, "η" );
    str = str.replace( /ί/g, "ι" );
    str = str.replace( /ϊ/g, "ι" );
    str = str.replace( /ΐ/g, "ι" );
    str = str.replace( /ό/g, "ο" );
    str = str.replace( /ύ/g, "υ" );
    str = str.replace( /ϋ/g, "υ" );
    str = str.replace( /ΰ/g, "υ" );
    str = str.replace( /ώ/g, "ω" );
  }

  return str;
}

/**
 * Countries
 *
 */
export const timezones = [
  {
    id: 1,
    code: 'CY',
    country: 'Cyprus',
    zone: 'Asia/Nicosia',
    offset: 10800,
    label: 'Cyprus (Asia/Nicosia)'
  },
  {
    id: 2,
    code: 'DE',
    country: 'Germany',
    zone: 'Europe/Berlin',
    offset: 7200,
    label: 'Germany (Europe/Berlin)'
  },
  {
    id: 3,
    code: 'DE',
    country: 'Germany',
    zone: 'Europe/Busingen',
    offset: 7200,
    label: 'Germany (Europe/Busingen)'
  },
  {
    id: 4,
    code: 'GR',
    country: 'Greece',
    zone: 'Europe/Athens',
    offset: 10800,
    label: 'Greece (Europe/Athens)'
  },
  {
    id: 5,
    code: 'GB',
    country: 'United Kingdom',
    zone: 'Europe/London',
    offset: 3600,
    label: 'United Kingdom (Europe/London)'
  },
];

/**
 * Countries
 *
 */
export const countries = [
  'Cyprus', 'Germany', 'Greece', 'United Kingdom'
];

/**
 * UI locales
 *
 */
export const uiLocales = [
    {label: 'Ελληνικά', val: 'el'},
    {label: 'English', val: 'en'}
];
