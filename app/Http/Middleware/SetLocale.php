<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $response = $next($request);

        if(auth('sanctum')->check()) {
            app()->setLocale(auth('sanctum')->user()->locale);
        } else {
            app()->setLocale('en');
        }

        return $response;
    }
}
