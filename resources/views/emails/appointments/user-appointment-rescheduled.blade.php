@component('mail::message')
# Your appointment was rescheduled

We inform you that your appointment was rescheduled for:

<strong>{{ $date }} {{ __('at') }} {{$time}} ({{ $timezone }})</strong>

<small>({{ __('Previous date was') }} {{$rescheduled_from}})</small>

You will be notified again 45 minutes before the session starts.

@component('mail::button', ['url' => url('/')])
{{ __('Login to view your appointment') }}
@endcomponent

Thank you,<br>
{{ config('app.name') }}
@endcomponent