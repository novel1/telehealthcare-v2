<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\NewFooHappendEvent;
use App\Listeners\SendMailForNewFoo;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        NewFooHappendEvent::class => [
            SendMailForNewFoo::class,
        ],

        \App\Events\NewAppointmentScheduledFromDoctorEvent::class => [
            \App\Listeners\SendUserNewAppointmentFromDoctorListener::class,
        ],

        \App\Events\UserBookedNewAppointmentEvent::class => [
            \App\Listeners\SendUserNewAppointmentInfoListener::class,
            \App\Listeners\SendDoctorNewAppointmentInfoListener::class,
        ],

        \App\Events\AppointmentRescheduledEvent::class => [
            \App\Listeners\SendUserAppointmentRescheduledListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
