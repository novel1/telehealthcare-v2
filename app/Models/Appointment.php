<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'transaction_id',
        'order_code',
        'status',
        'starts_at',
        'start_date',
        'start_time',
        'ends_at',
        'end_date',
        'end_time',
        'rescheduled_from',
    ];

    /**
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *
     */
    public function getStartsAtAttribute($value)
    {
        return Carbon::parse($value)->timezone('UTC');
    }

    /**
     *
     */
    public function getRescheduledFromAttribute($value)
    {
        return Carbon::parse($value)->timezone('UTC');
    }

    /**
     * Return corresponding color according to session status
     *
     * @param string $status Session status
     * @return string
     */
    function statusColor() {
        $colors = [
            'scheduled'        => '#FF2024',
            'rescheduled'      => '#FF2024',
            'canceled'         => '#70ff60',
            'pending'          => '#FF2024',
            'available'        => '#70ff60',
            'completed'        => '#FF9C24',
            'blue'             => '#4299e1',
            'unknown'          => '#9FA79E',
        ];

        return isset($colors[$this->status]) ? $colors[$this->status] : $colors['unknown'];
    }

}
