<?php

namespace App\Http\Controllers;

use App\Events\NewAppointmentScheduledFromDoctorEvent;
use App\Events\NewFooHappendEvent;
use App\Mail\AppointmentToggleMail;
use App\Models\Appointment;
use App\Models\Order;
use App\Models\User;
use App\Repositories\AppointmentRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Twilio\Jwt\AccessToken as TwilioAccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client as TwilioRestClient;

class AppointmentController extends Controller
{
    /**
     * [$appointmentRepo description]
     * @var [type]
     */
    private $appointmentRepo;

    /**
     * @param  AppointmentRepository
     * @return [type]
     */
    public function __construct(AppointmentRepository $appointmentRepo)
    {
        $this->appointmentRepo = $appointmentRepo;
    }

    /**
     * @return [type]
     */
    public function available()
    {
        return Inertia::render('Appointments/Book', [
            'appointments' => $this->appointmentRepo->availableForBooking(),
            'create_url' => URL::route('appointments.book'),
        ]);
    }

    /**
     * @return [type]
     */
    public function availability()
    {
        return Inertia::render('Appointments/Availability', [
            'timeslots' => 123,
            'appointments' => $this->appointmentRepo->reserved(),
            'create_url' => URL::route('appointments.availability'),
        ]);
    }

    /**
     * Set availability on datetime
     *
     * @param Request $request
     */
    public function setAvailability(Request $request)
    {
        $request->validate([
            'date' => 'required|date|unique:appointments,starts_at'
        ]);

        $date = Carbon::parse($request->date)->timezone('UTC');
        // $date = Carbon::parse('2020-11-04T14:00:00+02:00')->timezone('UTC');

        if($isBooked = $this->appointmentRepo->isBooked($date)) {
            return response(['isBooked' => $isBooked], 422);
        }

        $appointment = Appointment::create([
            'user_id' => $request->user()->id,
            'starts_at' => $date,
            'start_date' => $date->format('Y-m-d'),
            'start_time' => $date->format('H:s'),
            'status' => 'available',
            'transaction_id' => 'na',
            'order_code' => 'na'
        ]);

        return response([
            'appointments' => $this->appointmentRepo->reserved(),
            'isBooked' => $this->appointmentRepo->isBooked($date) ?: false
        ]);
    }

    /**
     * Get available apointments,
     * to reschedule a specific appointment
     *
     */
    public function reschedule(int $id)
    {
        $appointment = Appointment::findOrFail($id);
        $appointment->user;

        return Inertia::render('Appointments/Reschedule', [
            'appointments' => $this->appointmentRepo->available(),
            'appointment' => $appointment,
            'create_url' => URL::route('appointments.reschedule', $id),
        ]);
    }

    public function storeReschedule(Request $request) {

        $request->validate([
            'id' => 'required|numeric',
            'toId' => 'required|numeric',
        ]);

        $to_appointment   = Appointment::findOrFail($request->toId);

        if($to_appointment->status === 'completed' || $to_appointment === 'pending' || $to_appointment === 'scheduled') {
            return response(['error' => true], 422);
        }

        $appointment = Appointment::findOrFail($request->id);

        $appointment->starts_at        = $to_appointment->starts_at;
        $appointment->rescheduled_from = $appointment->starts_at;
        $appointment->save();

        // @todo delete or make available ?
        $to_appointment->delete();
        // $to_appointment->status = 'available';
        // $to_appointment->save();
        //

        event(new \App\Events\AppointmentRescheduledEvent($appointment));

        return ['appointment' => $appointment, 'to' => $to_appointment];
    }

    /**
     * Set availability on datetime
     *
     * @param Request $request
     * @return Response
     */
    public function removeAvailability(Request $request)
    {
        $request->validate([
            'date' => 'required|date',
        ]);

        $date = Carbon::parse($request->date)->timezone('UTC');
        // $date = Carbon::parse('2020-11-04T14:00:00+02:00')->timezone('UTC');

        if($isBooked = $this->appointmentRepo->isBooked($date)) {
            return response(['isBooked' => $isBooked], 422);
        }

        $appointment = Appointment::where('starts_at', $date)->firstOrFail();
        $appointment->delete();

        return response([
            'appointment' => $appointment,
        ]);
    }


    /**
     * Schedule an appointment for a user as a doctor
     *
     */
    public function scheduleStore(Request $request)
    {
        $request->validate([
            'date' => 'required|date',
            'user_id' => 'required|numeric'
        ]);

        $date = Carbon::parse($request->date)->timezone('UTC');

        $appointment = $this->appointmentRepo->isBookable($date);


        if(!$appointment) {
            return response(['error' => true], 422);
        }

        $appointment->user_id           = $request->user_id;
        $appointment->status            = 'scheduled';
        $appointment->transaction_id    = 'internal_book';
        $appointment->order_code        = 'internal_book';
        $appointment->save();

        event(new NewAppointmentScheduledFromDoctorEvent($appointment));

        return response(['appointment' => $appointment]);
    }

    /**
     * Schedule an appointment for a user as a doctor
     *
     */
    public function schedule($id)
    {
        $user = User::findOrFail($id);

        return Inertia::render('Appointments/Schedule', [
            'appointments' => $this->appointmentRepo->available(),
            'user_data' => $user,
            'create_url' => URL::route('appointments.availability.schedule', $id),
        ]);
    }

    /**
     * @return [type]
     */
    public function booked()
    {
        return Inertia::render('Appointments/Calendar', [
            'appointments' => $this->appointmentRepo->booked(),
            'create_url' => URL::route('appointments.calendar'),
        ]);
    }

    /**
     * @return [type]
     */
    public function userAppointments()
    {
        $appointments = $this->appointmentRepo->getUserScheduledAppointments();

        return Inertia::render('Appointments/User', [
            'appointments' => $appointments,
            'create_url' => URL::route('appointments.user'),
        ]);
    }

    /**
     * Destroy
     * @return [type]
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $appointment = Appointment::find($request->id);

        $order = Order::create([
            'user_id' => $appointment->user_id,
            'transaction_id' => $appointment->transaction_id,
            'order_code' => $appointment->order_code,
            'appointment_date' => $appointment->starts_at,
            'order_date' => $appointment->created_at,
            'status' => 'deleted',
            'deleted_by' => auth()->user()->id,
        ]);

        $appointment->delete();

        return response(['order' => $order, 'appointment' => $appointment]);
    }

    /**
     * cancel
     * @return [type]
     */
    public function cancel(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $appointment = Appointment::find($request->id);

        $order = Order::create([
            'user_id' => $appointment->user_id,
            'transaction_id' => $appointment->transaction_id,
            'order_code' => $appointment->order_code,
            'appointment_date' => $appointment->starts_at,
            'order_date' => $appointment->created_at,
            'status' => 'canceled',
            'deleted_by' => auth()->user()->id,
        ]);

        $appointment->status = 'available';
        $appointment->user_id = auth()->user()->id;
        $appointment->transaction_id = 'na';
        $appointment->order_code = 'na';
        $appointment->save();

        return response(['appointment' => $appointment]);
    }

    /**
     *
     *
     */
    private function getToken($identity, $room_name)
    {
        $account_sid       = env('MIX_TWILIO_ACCOUNT_SID');
        $api_key_id        = env('MIX_TWILIO_API_KEY_SID'); //tch-sessions2
        $api_key_secret    = env('MIX_TWILIO_API_KEY_SECRET');
        $twilio_auth_token = env('MIX_TWILIO_AUTH_TOKEN');

        // Create an Access Token
        $token = new TwilioAccessToken(
            $account_sid,
            $api_key_id,
            $api_key_secret,
            3600,
            $identity
        );

        // $twilio = new TwilioRestClient($account_sid, $twilio_auth_token );
        // $room = $twilio->video->v1->rooms
        //             ->create(["uniqueName" => "newapitest001"]);


        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom(trim($room_name));
        $token->addGrant($grant);

        return $token->toJWT();
    }

    /**
     *
     *
     */
    public function showRoomPreview(Request $request)
    {
        $app_id = (int) $request->id;

        $appointment = Appointment::find($app_id);
        $appointment->user;

        return Inertia::render('Appointments/Room/Preview', [
            'appointment' => $appointment,
            'appointment_id' => $app_id,
            'create_url' => URL::route('appointments.room.preview', $request->id),
        ]);
    }

    /**
     *
     *
     */
    public function showRoom(Request $request)
    {
        $appointment = Appointment::findOrFail($request->id);
        $identity    = trim(auth()->user()->name) . '_' . Str::random(8);
        $room_name   = Carbon::parse($appointment->starts_at)->timezone('UTC')->format('YmdHi') . $request->id;
        $token       = $this->getToken($identity, $room_name);
        $dt_now      = Carbon::parse(now())->timezone('UTC');

        return Inertia::render('Appointments/Room/Room', [
            'token' => $token,
            'room_name'  => $room_name,
            'appointment_id' => $request->id,
            'open' => $dt_now->greaterThan($appointment->starts_at),
            'create_url' => URL::route('appointments.room', $request->id),
        ]);
    }

    /**
     *
     *
     */
    public function complete(Request $request)
    {
        $request->validate([
            'id' => 'required|numeric'
        ]);

        $appointment = Appointment::find($request->id);

        $dt_now = Carbon::parse(now())->timezone('UTC');

        if($dt_now->greaterThan($appointment->starts_at)) {
            $appointment->status = 'completed';
            $appointment->save();
            return response(['success' => true, 'appointment' => $appointment]);
        }

        return response(['error' => true, 'appointment' => $appointment]);
    }
}
