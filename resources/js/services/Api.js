import axios from 'axios';

// @todo Separate to different files (e.g. EventsApi.js)
class Api {

  constructor(apiBase = null) {
    this.apiBase = apiBase || process.env.MIX_API_BASE;
    this.req = this.createAxiosInstance();
  }

  //--------------------------------------------------
  // Actions
  get(endpoint, params) {
    params = params || null;
    return this.req.get(endpoint, params);
  }

  post(endpoint, data) {
    return this.req.post(endpoint, data);
  }

  delete(endpoint) {
    return this.req.delete(endpoint);
  }

  patch(endpoint) {
    return this.req.patch(endpoint);
  }

  put(endpoint, data) {
    return this.req.put(endpoint, data);
  }

  createAxiosInstance() {
    const instance = axios.create({
      baseURL: this.apiBase,
      withCredentials: true,
      headers: {
        'x-requested-with': 'XMLHttpRequest',
        'content-type': 'application/json',
        'accept': 'application/json',
      },
      responseType: 'json'
    });

    return instance;
  }
}

export default Api;
