<?php

namespace App\Repositories;

use App\Models\Appointment;
use Carbon\Carbon;

class AppointmentRepository
{
    /**
     * All vailable for booking, including today date
     *
     * @todo  Maybe set start time if time is later than now?
     * @return Appointment
     */
    public function available()
    {
        return Appointment::where('status', 'available')
                ->get()
                ->map(function($appt) {
                    return $this->toCalendarData($appt);
                });
    }

    /**
     * Available for booking 3 days from now (time for doctor to prepare)
     * We dont care about start time since we are 3 days in the future
     *
     * @return Appointment
     */
    public function availableForBooking()
    {
        $three_days_from_now = now()->addDays(3)->timezone('UTC');

        return Appointment::where('status', 'available')
                ->where('starts_at', '>=', $three_days_from_now)
                ->get()
                ->map(function($appt) {
                    return $this->toCalendarData($appt);
                });
    }

    /**
     * Booked appointements
     *
     * @return Appointment
     */
    public function booked()
    {
        return Appointment::where('status', 'scheduled')
                ->orWhere('status', 'rescheduled')
                ->get()
                ->map(function($appt) {
                    return $this->toCalendarData($appt);
                });
    }

    /**
     * Booked appointements
     *
     * @return Appointment
     */
    public function bookedOrAvailable()
    {
        return Appointment::where('status', 'scheduled')
                ->orWhere('status', 'rescheduled')
                ->orWhere('status', 'available')
                ->orWhere('status', 'canceled')
                ->get()
                ->map(function($appt) {
                    return $this->toCalendarData($appt);
                });
    }

    /**
     * Get all appontments that are not
     * available for seting available to book
     *
     * @return Appointment
     */
    public function reserved()
    {
        return Appointment::where('status', 'scheduled')
                ->orWhere('status', 'rescheduled')
                ->orWhere('status', 'available')
                ->orWhere('status', 'canceled')
                ->orWhere('status', 'available')
                ->orWhere('status', 'pending')
                ->orWhere('status', 'completed')
                ->get()
                ->map(function($appt) {
                    return $this->toCalendarData($appt);
                });
    }

    /**
     * Booked appointements
     *
     * @return Appointment
     */
    public function isBooked($date)
    {
        return Appointment::where('starts_at', $date)
                ->where(function ($query) {
                    $query->where('status', '=', 'scheduled')
                        ->orWhere('status', '=', 'rescheduled')
                        ->orWhere('status', '=', 'pending')
                        ->orWhere('status', '=', 'completed');
                })
                ->first();
    }

    /**
     * Bookable appointements
     * (Available for user to book)
     *
     * @return Appointment
     */
    public function isBookable($date)
    {
        return Appointment::where('starts_at', $date)
                ->where(function ($query) {
                    $query->where('status', '!=', 'scheduled')
                        ->orWhere('status', '!=', 'rescheduled')
                        ->orWhere('status', '!=', 'completed')
                        ->orWhere('status', '!=', 'pending');
                })
                ->first();
    }

    /**
     * [getUserScheduledAppointments description]
     * @return [type] [description]
     */
    public function getUserScheduledAppointments() {

        return  Appointment::where('user_id', auth()->user()->id)
                    ->where('status', 'scheduled')
                    ->get()
                    ->map(function($item, $key) {
                        return $this->toCalendarData($item);
                    });
    }

    /**
     * Map collection to FullCander data
     *
     * @param Appointment $appt
     * @return Array
     */
    public function toCalendarData(Appointment $appt) {
        return [
            'id' => $appt->id,
            'start'          => $appt->starts_at->timezone('UTC'),
            'end'            => $appt->starts_at->timezone('UTC')->addhours(1),
            'description'    => 'Available timeslot',
            'classNames'     => 'app-timeslot-status-' . $appt->status,
            'extendedProps'  => [
                'start_date' => $appt->start_date,
                'start_time' => $appt->start_time,
                'user'       => $appt->user
            ],
            'backgroundColor' => ''
        ];
    }
}
