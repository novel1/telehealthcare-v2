@component('mail::message')
# Introduction

---
{{$date}}<br>
{{$status}}<br>
{{$user_name}}<br>
{{$timezone}}<br>
---

@component('mail::button', ['url' => url('/')])
telehealth.drgeorgallas.com
@endcomponent

Ευχαριστούμε,<br>
{{ config('app.name') }}
@endcomponent
