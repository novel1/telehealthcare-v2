<?php

namespace App\Listeners;

use App\Events\AppointmentRescheduledEvent;
use App\Mail\SendUserAppointmentRescheduledMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendUserAppointmentRescheduledListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppointmentRescheduled  $event
     * @return void
     */
    public function handle(AppointmentRescheduledEvent $event)
    {
        Mail::to($event->appointment->user->email)->send(new SendUserAppointmentRescheduledMail($event->appointment));
    }
}
