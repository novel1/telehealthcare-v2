<?php

namespace App\Services;

class VivaPaymentService {

  private $amount;

  private $callback_source_code;

  private $merchant_id;

  private $api_key;

  private $sandbox_api_url;

  private $api_url;

  private $allow_reoccuring;

  private $request_lang;

  private $post_args;

  public  $order_body;

  public function __construct()
  {
    $this->merchant_id          = env('MIX_VIVAP_MERCH_ID');
    $this->api_key              = env('MIX_VIVAP_API_KEY');
    $this->api_url              = env('MIX_VIVAP_API_URL');
    $this->allow_reoccuring     = 'false';
    $this->request_lang         = 'el-GR';
    $this->callback_source_code = env('MIX_VIVAP_CALLBACK_CODE');
  }

  private function setDemoEnv() {

  }

  /**
   * createOrder
   * 
   */
  public function createOrder($amount) {
    $this->amount = $amount;

    

    $this->postArgs();
    return $this->postOrder();
  }

  /**
   * postOrder
   * 
   */
  public function postOrder() 
  {
        // Get the curl session object
        $ch = curl_init();
        // Set the POST options.
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->post_args);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $this->merchant_id.':'.$this->api_key);
        curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');

        // Do the POST and then close the session
        $response = curl_exec($ch);

        // Separate Header from Body
        $header_len = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $res_header  = substr($response, 0, $header_len);
        $res_body    =  substr($response, $header_len);

        curl_close($ch);

        return $this->parseOrderResponse($res_body, $res_header);
  }

  /**
   * parseOrderResponse
   * 
   */
  public function parseOrderResponse($response_body, $response_header) 
  {
    try {
      $response = json_decode($response_body);
      if(is_object($response)) {
        if (isset($response->ErrorCode) && $response->ErrorCode == 0) {
          return json_decode($response_body, true);
        }
        elseif(isset($response->Message)) {
          $error = $response->Message;
        }
        else {
          $error = $response_body;
        }
      } 
      else {
          preg_match('#^HTTP/1.(?:0|1) [\d]{3} (.*)$#m', $response_header, $match);
          $error = trim($match[1]);
      }
    } catch(\Exception $e) {
      $error = $e->getMessage();
    }

    throw new \Exception('Vivapayment Create Order Error: '.$error);
  }

  public function makePayment($orderId)
  {

  }

  private function postArgs() {
    $this->post_args = 'Amount='.urlencode($this->amount).'&AllowRecurring='.$this->allow_reoccuring.'&RequestLang='.$this->request_lang.'&SourceCode='.$this->callback_source_code;
  }

  public function errors() {

  }
}