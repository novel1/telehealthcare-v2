@component('mail::message')
# You have a live video session with Dr. Georgalas
<br/><br/>
You session is scheduled for: {{$date}} {{__('at')}} {{$time}} ({{$timezone}})<br/>
<br/>

@component('mail::button', ['url' => url('/')])
{{__('Login to view your appointment')}}
@endcomponent

Thank you,<br>
{{ config('app.name') }}
@endcomponent
