<?php

namespace App\Listeners;

use App\Events\UserBookedNewAppointmentEvent;
use App\Mail\SendDoctorNewAppointmentInfoMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendDoctorNewAppointmentInfoListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserBookedNewAppointment  $event
     * @return void
     */
    public function handle(UserBookedNewAppointmentEvent $event)
    {
        $admin_email = get_setting('admin_email');
        Mail::to($admin_email)->send(new SendDoctorNewAppointmentInfoMail($event->appointment));
    }
}
