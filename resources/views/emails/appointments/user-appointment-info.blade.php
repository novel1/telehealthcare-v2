@component('mail::message')
# Thank you for booking a live video appointment with Dr. Georgallas

Your transaction id (made with Stripe):
<br/>
<b>{{ $transaction_id }}</b>
<br/><br/>
You session is scheduled for:<br>
{{ $date }} {{ __('at') }} {{$time}} ({{ $timezone }})<br/>
<br/>
You will be notified again 45 minutes before the session starts.

@component('mail::button', ['url' => url('/')])
{{ __('Login to view your appointment') }}
@endcomponent

Thank you,<br>
{{ config('app.name') }}
@endcomponent

