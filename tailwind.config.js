const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  purge: [
    './vendor/laravel/jetstream/**/*.blade.php',
    './storage/framework/views/*.php',
    './resources/views/**/*.blade.php',
    './resources/js/**/*.vue',
  ],

  corePlugins: {
    fontFamily: false,
  },

  theme: {
    backgroundColor: theme => ({
      ...theme('colors'),
      'blue-001': '#f7fafc',
      'blue-002': '#d1dee6',
    }),
      borderColor: theme => ({
        ...theme('colors'),
        'gray-001': '#e0e3e8',
        'gray-002': '#d4d5d8'
    }),
    extend: {
      colors: {
        appBrand: 'hsl(38, 63%, 45%)',
        appBrandDark: '#704f2e',
        appBrandLight: 'hsl(50, 18%, 94%)',
        appBlack: '#000000',
        appBtn: '#704f2e',
        appBtnHover: '#BA852B',
      },
      zIndex: {
        auto: 'auto',
        '2': '2',
        '99': '99',
        '9998': '9998',
        '9999': '9999',
      },
      spacing: {
        '18': '4.5rem',
        '72': '18rem',
        '80': '20rem',
        '84': '21rem',
        '96': '24rem',
        'xx': '112rem'
      },
      maxWidth: {
        '8xl': '88rem'
      },
      boxShadow: {
        '3xl': '0 35px 60px -15px rgba(0, 0, 0, 0.6)',
      },
      textColor: {
        'blue2': '#5373a7',
      },
    },
  },

    variants: {
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
    },

    plugins: [require('@tailwindcss/ui')],
};
