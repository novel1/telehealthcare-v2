<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
        'value',
    ];

    public static function getSetting(string $key) {
    	return self::where('key', trim($key))->first() ?: null;
    }

    public static function getAllSettings(string $key) {
    	return $this->all();
    }
}
