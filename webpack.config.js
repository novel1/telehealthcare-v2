const path = require('path');

module.exports = {
  resolve: {
    alias: {
      '@form': path.resolve('resources/js/components/Form'),
      '@': path.resolve('resources/js'),
    },
  },
};
