require('./bootstrap');
require('intersection-observer');

import Vue from 'vue';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';
import Notifications from 'vue-notification';

import Api from '@/services/Api';

import Button from '@/Jetstream/Button';
import DangerButton from '@/Jetstream/DangerButton';
import SecondaryButton from '@/Jetstream/SecondaryButton';
import DialogModal from '@/Jetstream/DialogModal';

import Card from '@/components/Card';
import OutlineButton from '@/components/OutlineButton';
import LinkButton from '@/components/LinkButton';

import lang from '@/services/lang';
import { slugify, removeAccs } from '@/services/utils';

Vue.mixin({ methods: { route } });
Vue.use(InertiaApp);
Vue.use(InertiaForm);
Vue.use(PortalVue);
Vue.use(Notifications);

Vue.component('Card', Card);
Vue.component('Button', Button);
Vue.component('DangerButton', DangerButton);
Vue.component('SecondaryButton', SecondaryButton);
Vue.component('OutlineButton', OutlineButton);
Vue.component('DialogModal', DialogModal);
Vue.component('LinkButton', LinkButton);

Vue.prototype.$tr = lang;
Vue.prototype.$slug = slugify;
Vue.prototype.$api = new Api;

Vue.filter('uppercf', function (value) {
    if (!value) return ''
    return removeAccs(value.toString().charAt(0).toUpperCase() + value.slice(1));
});

Vue.filter('upperc', function (value) {
    if (!value) return ''
    return removeAccs(value.toString().toUpperCase());
});

Vue.filter('accents', function (value) {
    if (!value) return '';
    return removeAccs(value);
});

const app = document.getElementById('app');

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);
