<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\HourlyAppointmentReminderMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class HourlyAppointmentReminderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now_plus_hour = Carbon::parse(now())->timezone('UTC')->addHours(1)->format('Y-m-d H:00:00');

        $appointments = Appointment::where('starts_at', $now_plus_hour)
            ->where('status', 'scheduled')
            ->where('reminder_sent', 0)
            ->get();

        if($appointments) {
            foreach ($appointments as $appointment) {
                $data = [
                    'date' => Carbon::parse($appointment->starts_at)->timezone($appointment->user->timezone)->locale($appointment->user->locale)->toFormattedDateString(),
                    'time' => Carbon::parse($appointment->starts_at)->timezone($appointment->user->timezone)->format('H:i'),
                    'user_name' => $appointment->user->name,
                    'timezone' => $appointment->user->timezone,
                ];

                Mail::to($appointment->user->email)
                    ->send(new HourlyAppointmentReminderMail($data));

                $appointment->reminder_sent = 1;
                $appointment->save();
            }
        }

        return null;
    }
}
