<?php

namespace Database\Seeders;

use App\Models\Appointment;
use Illuminate\Database\Seeder;


class AppointmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // $appt = new Appointment();
        // $appt->user_id = 2;
        // $appt->transaction_id = 'na';
        // $appt->order_code = 'na';
        // $appt->status = 'available';
        // $appt->starts_at = '2021-07-12 10:00:00';
        // $appt->start_time = '10:00:00';
        // $appt->start_date = '2021-06-12';
        // $appt->created_at = now();
        // $appt->updated_at = now();
        // $appt->save();

        // $appt = new Appointment();
        // $appt->user_id = 2;
        // $appt->transaction_id = 'na';
        // $appt->order_code = 'na';
        // $appt->status = 'available';
        // $appt->starts_at = '2021-07-12 11:00:00';
        // $appt->start_time = '11:00:00';
        // $appt->start_date = '2021-07-12';
        // $appt->created_at = now();
        // $appt->updated_at = now();
        // $appt->save();

        // $appt = new Appointment();
        // $appt->user_id = 2;
        // $appt->transaction_id = 'na';
        // $appt->order_code = 'na';
        // $appt->status = 'available';
        // $appt->starts_at = '2021-07-12 13:00:00';
        // $appt->start_time = '11:00:00';
        // $appt->start_date = '2021-07-12';
        // $appt->created_at = now();
        // $appt->updated_at = now();
        // $appt->save();

        $session = new Appointment();
        $session->user_id = 3;
        $session->transaction_id = 'internal_book';
        $session->order_code = 'internal_book';
        $session->status = 'scheduled';
        $session->starts_at = '2021-07-13 12:00:00';
        $session->start_time = '12:00:00';
        $session->start_date = '2021-07-13';
        $session->created_at = now();
        $session->updated_at = now();
        $session->save();

        // $session = new Appointment();
        // $session->user_id = 3;
        // $session->transaction_id = 'na';
        // $session->order_code = '234234234234';
        // $session->status = 'pending';
        // $session->starts_at = '2021-07-14 10:00';
        // $session->start_time = '10:00:00';
        // $session->start_date = '2021-07-14';
        // $session->created_at = now();
        // $session->updated_at = now();
        // $session->save();
    }
}
