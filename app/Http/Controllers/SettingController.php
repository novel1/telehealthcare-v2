<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Inertia\Inertia;

class SettingController extends Controller
{
	/**
	 * [edit description]
	 *
	 * @return [type] [description]
	 */
	public function edit() {
        $settingsCollection = Setting::all();

        $settings = [];

        if($settingsCollection) {
            foreach($settingsCollection as $set) {
                $settings[$set->key] = $set->value;
            }
        }

	    return Inertia::render('Settings', [
            'settings' => $settings,
	   	    'create_url' => URL::route('settings.edit'),
		]);
	}

	/**
	 * [store description]
	 *
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
    public function update(Request $request) {
	    $request->validate([
	        'admin_email' => 'required|email',
	        'appointment_price' => 'required|numeric',
            'product_id' => 'required|string',
            'product_id_test' => 'required|string',
	    ]);

	    foreach($request->all() as $key => $val) {
	    	$setting = Setting::where('key', $key)->first();
	    	if($setting) {
	    		$setting->value = $val;
	    		$setting->save();
	    	}
	    }

	    return response(['success' => true]);
    }
}
