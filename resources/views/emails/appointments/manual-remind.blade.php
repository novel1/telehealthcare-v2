@component('mail::message')
# Υπενθήμιση ραντεβού

Σας υπενθημίζουμε οτι σήμερα στις {{$time}} ({{$timezone}}) έχετε online ραντεβού με τον Δρ.Γεωργαλλά.

@component('mail::button', ['url' => url('/')])
telehealth.drgeorgallas.com
@endcomponent

Ευχαριστούμε,<br>
{{ config('app.name') }}
@endcomponent
