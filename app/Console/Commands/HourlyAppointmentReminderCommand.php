<?php

namespace App\Console\Commands;

use \App\Jobs\HourlyAppointmentReminderJob;
use Illuminate\Console\Command;

class HourlyAppointmentReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:appointment_reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send appointment reminder to users one hour before start';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new HourlyAppointmentReminderJob());
    }
}
